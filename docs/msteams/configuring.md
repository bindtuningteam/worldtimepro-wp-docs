1. On the Tab click on the dropdown and then **Settings**. If is the first time adding the **World Time Pro**, you can skip this process. 

    ![settings_delete.png](../images/msteams/setting_edit.png)

   
2. Configure the web part according to the settings described in the **[Web Part Properties Section](./general.md)**;

    ![08.configurepanel.png](../images/modern/06.options.png)

3. The properties are saved automatically, so when you're done click to close the Properties. 