### 24H Format

With this option **Activated** the time will show in the 24h hours instead of the default with AM and PM.

--------
### Allow users to add personalized clocks

With this option **Activated**, the user will be able to add his own clock. This will unlock a functionality when you have the Web Part added in the page with 1 clock set up, you'll be able to add more on the page.

Mouse over the World Time Pro Web Part zone and a plus icon **[+]** will show. Click on that icon and a new Clock will be added to the page.

![Own clock.png](https://bitbucket.org/repo/6499g4B/images/1066023456-Own%20clock.png)

You can then configurate your own Clock by clicking on **Change Location**

![Own clock Settings.png](https://bitbucket.org/repo/6499g4B/images/4113774144-Own%20clock%20Settings.png)

--------
### Include local clock

With this option active, it'll always display a **Local Clock** based on the time and date that you've on your desktop.

---
### Title

If you click on the Title of the Web Part on the page, you can change the Title to the Web Part that will be shown. When you insert a text there it'll show on the page the **Title**, otherwise, nothing will be visible in the page.

![02.appearance.text.png](../images/msteams/01.appearance.png)

----
### Title Link 

Note this option is only valid if you have a Title on the Web Part.<br>
On the **Title** of the Web Part, you can add an URL to redirect you to any page. The Web Part will always open the new URL in a new tab of your browser and that option is only visible if you're not in **Edit** mode.s

![Add section](../images/modern/04.advanced.png)