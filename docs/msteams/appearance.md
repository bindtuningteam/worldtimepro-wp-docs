### Rigth to Left

Using this option will change the web part's text orientation to go from right to left. 
The forms will not be affected.


![01.appearance.color.png](../images/msteams/01.appearance.png)