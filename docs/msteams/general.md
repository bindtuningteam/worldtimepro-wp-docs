On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Default Time Zones](./options.md)
- [Layout Options](./layout.md)
- [Web Part Appearance](./appearance.md)
- [Advanced Options](./advanced.md)
- [Web Part Messages](./message.md)


![06.options](../images/modern/06.options.png)