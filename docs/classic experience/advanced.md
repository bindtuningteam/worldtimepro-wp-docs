### Language

From here you can select which language you want to be displayed by the web part. This will also translate the forms, but a refresh may be required before you see the changes.

By default, the BindTuning Web Part provides the language for:

- English
- Portuguese

If you'd like to localize the web part to your own language, please refer to [this article](https://support.bindtuning.com/hc/en-us/articles/115004585263).

--------
### 24H Format

With this option **Activated** the time will show in the 24h hours instead of the default with AM and PM.

--------
### Allow users to add personalized clocks

With this option **Activated**, the user will be able to add his own clock. This will unlock a functionality when you have the Web Part added in the page with 1 clock set up, you'll be able to add more on the page.

Mouse over the World Time Pro Web Part zone and a plus icon **[+]** will show. Click on that icon and a new Clock will be added to the page.

![Own clock.png](https://bitbucket.org/repo/6499g4B/images/1066023456-Own%20clock.png)

You can then configurate your own Clock by clicking on **Change Location**

![Own clock Settings.png](https://bitbucket.org/repo/6499g4B/images/4113774144-Own%20clock%20Settings.png)

--------
### Include local clock

With this option active, it'll always display a **Local Clock** based on the time and date that you've on your desktop.

![Layout options.PNG](../images/classic/13.advanced.png)
