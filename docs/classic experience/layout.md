<p class="alert alert-success">If you  page don't have space to show all the clocks on the World Time Pro Web part zone, it will create a Slider.</p>

----
### Face

You can choose between two options: 

- **Digital**
- **Analog**

----
### Shape

You can choose between two options for the shape of the borders of the watch: 

- **Rounded**
- **Squared**

----
### Skin

You can choose between three options for the colors: 

- **Light (Default)** 
- **Light (All Numbers)** 
- **Dark** 

----
### Layout

You can choose between two options to show the layouts: 

- **Horizontal**
- **Vertical**  

----
### Size

You can choose between three options for the size of the Clock: 

- **Normal**
- **Small**
- **Tiny**

![Layout options.PNG](../images/classic/12.layouts.png)