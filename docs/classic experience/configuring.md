1. Click the **BindTuning Tab** on the Ribbon and click on **Edit Web Parts**.

    ![bindtuning-tab](../images/classic/11.edit.wp.png)

    The property pane will appear to the left of the web part;

2. Click the cogwheel icon. This will open the properties form;

3. **Configure** the web part according to the settings described in the **[Web Part Properties Section](./general.md)**;
   
4. When you're done, click the **Save** button and the web part will reload.

    ![form-save](../images/classic/10.save.png)

<p class="alert alert-info"><b>Note:</b> that the Appearence options won't be visible until you reload the page.</p>