### Search by Time Zone

If you activate this option, you will be able to search for a time zone instead of Search by city.

---
### Search a city/timezone

Enter the city/timezone where you want to display the Time. When you start to type the name will appear the searched cities/timezones.

---
### Set a name for your clock

Define a name to be shown on top of the clock that you're creating. 

<p class="alert alert-info">Define which city users should see by default. Leaving this blank will default to <b>New York</b>.</p>

![advanced.PNG](../images/modern/03.provideroptions.gif)